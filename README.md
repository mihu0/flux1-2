Flux upgrade demo
-----------------

Repository contains three branches:

* `flux1` – environment for flux1 setup
* `flux1_to_2` – script to uninstall flux 1 automation
* `flux2` – environment for flux2 setup

See readmes in the branches how to bootstrap the envs.
